# HELPDESK SYSAID HELPER #
Tools to shorten time in the process of generating case ID of Helpdesk@PSU

* Version 0.1.1

### How do I get start? ###

* after cloning git, rename *.demo by removing .demo extension
* Config user profile in config.php
* Run gen.php
* Do what you want
* That's all!

### Contribution guidelines ###

* all commitments will be automatically deploy to MS Azure

### Who do I talk to? ###

* Repo owner: Chalermkiat Lertnittiyanama (Sun)
* Contact: chalermkiat.lert@gmail.com