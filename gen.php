<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>HLPDSK_SYSAID_X1</title>
  
 

  
      <link rel="stylesheet" href="css/style.css">

<style type="text/css">

h1,h2 {
  font-family: sukhumvit;
}
                        #header{
                                width:100%;
                                margin:auto;
                        }
                        #left{
                                width:31%;
                                float:left;
                                margin:auto auto auto 2%;
                        }
                        #right{
                                float:right;
                                width:31%;
                                margin:auto 2% auto auto;
                        }
                        #middle{
                                left:0;
                                right:0;
                                margin:auto;
                                padding-top: 1px;
                                width: 31%;
                        }
                        #footer{
                                width:100%;
                                margin:auto;
                        }
                        #left0{
                                width:45%;
                                float:left;
                                margin:auto auto auto 2%;
                        }
                        #right0{
                                float:right;
                                width:45%;
                                margin:auto 2% auto auto;
                        }
                </style>
</head>

<body>
<form action="execute.php" method="post">

<span style="display: none;">
<?php 

//extract ramdom redirected server from SIS PSU
$sis_serv_num_url="http://sis.psu.ac.th";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $sis_serv_num_url);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Must be set to true so that PHP follows any "Location:" header
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$a = curl_exec($ch); // $a will contain all headers

$sis_serv_num_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); // This is what you need, it will return you the last effective URL



$sis_server_number = explode("sis-hatyai", $sis_serv_num_url);
$sis_server_number = explode(".", $sis_server_number[1]);
$sis_server_number = $sis_server_number[0];

$sis_server_config_url = explode("/WebRegist2005/", $sis_serv_num_url);
$sis_server_config_url = $sis_server_config_url[0];

?>
<input type="hidden" name="server_url" value="<?php echo $sis_server_config_url; ?>" />
	
</span>



<h1>ระบบช่วยกรอก SysAid</h1>
<span> SIS Server Number: <?php echo $sis_server_number; ?> </span>
                <div id="left0"><div class="container">
  
    <h2>ข้อมูลลูกค้า</h2>
    <div class="form-group">
      <input type="text" name="id" required="required"/>
      <label class="control-label" for="input">PSU ID</label><i class="bar"></i>
    
    </div>
    <div class="form-group">
      <input type="text" name="year" required="required"/>
      <label class="control-label" for="input">YEAR</label><i class="bar"></i>
    </div>
    <div class="form-group">
      <input type="text" name="tel" required="required"/>
      <label class="control-label" for="input">Tel</label><i class="bar"></i>
    </div>
    <div class="form-radio">
      <div class="radio">
        <label>
          <input type="radio" name="customer_type" value="Student of PSU: นักศึกษา ม.อ." checked="checked"/><i class="helper"></i>นักศึกษา
        </label>
      </div>
      <div class="radio">
        <label>
          <input type="radio" name="customer_type" value="Staff of PSU: บุคลากร ม.อ."/><i class="helper"></i>บุคลากร
        </label>
      </div>
      <br><br>
</div>
                </div> </div>


               





                <div id="right0" style="padding-top: 1px;">
                <div class="container">
  <h2>รายละเอียดปัญหา</h2>
    <div class="form-group">
<input type="text" name="problem" required />
      <label class="control-label" for="input">Problem Topic</label><i class="bar"></i>
    </div>
    <div class="form-group">
<textarea name="detail" required></textarea>
      <label class="control-label" for="textarea">Detail</label><i class="bar"></i>
    </div>
    <div class="form-group">
<textarea name="solution" required></textarea>
      <label class="control-label" for="textarea">Solution</label><i class="bar"></i>
    </div>
    <br><br><br>
</div>
       </div>
       <div style="clear: both"></div><br><br><hr/>



       <h1>ประเภทของปัญหา</h1>

<div id="left">
<div class="container">
    <div class="form-radio">
     <div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Android Phone/Tablet"required /><i class="helper"></i>Android Phone/Tablet </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Anti-Virus"/><i class="helper"></i>Anti-Virus </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Application/ Program"/><i class="helper"></i>Application/ Program </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Cloud Storage"/><i class="helper"></i>Cloud Storage </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Fixed IP Address"/><i class="helper"></i>Fixed IP Address </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Flash Drive"/><i class="helper"></i>Flash Drive </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Internet"/><i class="helper"></i>Internet </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="iPhone/iPad"/><i class="helper"></i>iPhone/iPad </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="No Category: ยังแยกประเภทไม่ได้"/><i class="helper"></i>No Category: ยังแยกประเภทไม่ได้</label>
      </div>
    </div></div></div>
<div id="right">
      <div class="container">
      <div class="form-radio">
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="เครื่องค้างบ่อย: Hang"/><i class="helper"></i>เครื่องค้างบ่อย: Hang </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="เครื่องทำงานช้า"/><i class="helper"></i>เครื่องทำงานช้า </label>
      </div>
<div class="radio">  
        <label> <input type="radio" name="thirdLevelCategory" value="เครื่องเปิดไม่ติด"/><i class="helper"></i>เครื่องเปิดไม่ติด </label>
      </div>

 <div class="button-container">
    <button  type="submit" class="button"><span>Submit</span></button>

  </div>


    </div></div></div>

    <div id="middle">
    <div class="container">
    <div class ="form-radio">
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Proxy-Medicine"/><i class="helper"></i>Proxy-Medicine </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Proxy-Science"/><i class="helper"></i>Proxy-Science </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="VPN"/><i class="helper"></i>VPN </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Web Browser"/><i class="helper"></i>Web Browser </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="WiFi"/><i class="helper"></i>WiFi </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Windows Activation"/><i class="helper"></i>Windows Activation </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Windows Boot"/><i class="helper"></i>Windows Boot </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="Windows Update"/><i class="helper"></i>Windows Update </label>
      </div>
<div class="radio">
        <label> <input type="radio" name="thirdLevelCategory" value="จอภาพแสดงสีผิดเพี้ยน"/><i class="helper"></i>จอภาพแสดงสีผิดเพี้ยน </label>
      </div>
      </form>

      </div></div>
                <div id="footer">
                    <!-- FOOTER CONTENT GOES HERE -->
                </div>
  

</body>
</html>
